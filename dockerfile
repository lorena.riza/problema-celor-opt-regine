FROM gcc:latest
WORKDIR usr\src\problema_reginelor
COPY problema_reginelor.cpp .
RUN gcc -o problema_reginelor problema_reginelor.cpp -lstdc++
CMD ["./problema_reginelor"]
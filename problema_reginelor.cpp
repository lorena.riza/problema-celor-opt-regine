#include<iostream>

using namespace std;

int safe (int chess[8][8], int row, int column, int n)
{
    int i, j;

    for (i = 0; i < row; i++)
        if (chess[i][column] == 1)
            return 0;

	for (int i = row, j = column; i >= 0 && j >= 0; --i, --j) {
		if (chess[i][j] == 1)
			return 0;
	}
    
	for (int i = row, j = column; i >= 0 && j < n; --i, ++j) {
		if (chess[i][j] == 1)
			return 0;
	}

    return 1;
}

int random (int minim, int maxim)
{
    return minim + rand() % (maxim - minim +1 );
}

int solve_queens (int chess [8][8], int row, int n, int start_column)
{
    int column, i;

    if (row == n)
        return 1;

    for (i = 0; i < n; i++)
    {
            column=(start_column + random (0, n-1)) %n;

            if (safe(chess, row, column, n))
             {
                chess[row][column] = 1;
    
                 if (solve_queens(chess, row + 1, n, (column + 1) % n))
                 return 1;

              chess[row][column] = 0;
            }
    }

    return 0;
}

void print(int chess[8][8],  int n)
{
    int column, i, j;

    cout<<" ";
    for (column = 0; column < n; column++)
        cout<< static_cast <char> ('a' + column)<<" ";
    
    cout<< endl<< "- - - - - - - - -"<<endl;

    for (i = 0; i < n; i++)
    {
        cout<< n - i << "|";
        for (j = 0; j < n; j++)
            if(chess[i][j] == 1)
                cout<< "Q|";
            else
                cout<< "_|";
        cout<<endl;
    }
    cout<< "- - - - - - - - -"<<endl;
}

void solve(int n)
{
    int chess[8][8] = { 0 };

    if(!solve_queens(chess, 0, n, 0))
    {
        cout<< "Nu exista solutie."<<endl;
        return;
    }

    print(chess, n);
}

int main ()
{
    int opt;

    do
    {
        cout<<"Meniu: "<<endl;
        cout<<"1. Generarea unei solutii"<<endl;
        cout<<"2. Iesire"<<endl;
        cout<<"Dati optiunea: ";
         cin >> opt;
        switch (opt)
        {
        case 1: solve(8);
            break;
        case 2: cout<<"Ati iesit din program.";
                 exit(0);
            break;
        default: cout<<"Optiune invalida! Reintroduceti optiunea.";
            break;
        }
    } while (opt != 2);

    return 0;
}
<h1>Problema Celor Opt Regine

<h2>Descriere

Problema celor opt regine este un puzzle ce constră în plasarea a opt regine pe o tablă de șah (o matrice de 8x8), astfel incât oricare două regine să nu se poată ataca una pe cealaltă. Din această cerință rezultă că oricare două regine trebuie nu au voie să fie pe aceeași coloană, linie sau diagonală.

Această problemă este un caz specific al problemei celor N regine, problemă în care trebuie să fie N regine ce nu se pot ataca între ele pe o tablă de NxN. Există soluții pentru toate numerele naturale, cu excepția N=2 si N=3, însă se știe numărul exact de soluții numai pentru N<=27. Pentru cazul nostru, în care N=8, există 92 de soluții, dintre care 12 soluții sunt distincte. Acest lucru se întâmplă datorită simetriei.

<h2>IMPORTANT! docker image

În loc de comanda:
>docker run map-problema-reginelor(/nume_pus)

Scrieți comanda:
>docker run -it map-problema-reginelor(/nume_pus)

Prima comandă va face meniul să intre într-o buclă infinită, însă a doua comandă funcționează așa cum trebuie deoarece:
 - comanda '-it' este folosită pentru a da run unui contaner Docker ce are nevoie de input interactiv

<h2>Mod de Lucru

Codul utilizează limbajul C++ și biblioteca standard C++ (iostream) și se folosește o abordare aleatorie pentru plasarea reginelor pe tablă.

**Funcția safe**

Această funcție verifică dacă o anumită poziție pe tablă este sigură pentru plasarea unei regine.

>int safe (int chess[8][8], int row, int column, int n)
 - chess[8][8] reprezintă tabla de șah pe care sunt plasate reginele
 - row reprezintă liniile
 - column reprezintă coloanele
 - n reprezintă ordinul matricii, cât și numărul de regine, în cazul nostru specific, 8

Se verifică dacă există vreo regină pe aceeași coloană.
>for (i = 0; i < row; i++)
>        if (chess[i][column] == 1)
>            return 0;

Se verifică dacă există vreo regină pe diagonala stânga-sus.
>for (int i = row, j = column; i >= 0 && j >= 0; --i, --j) 
>{
>		if (chess[i][j] == 1)
>			return 0;
>}

Se verifică dacă există vreo regină pe diagonala dreapta-sus.
>for (int i = row, j = column; i >= 0 && j < n; --i, ++j) 
>{
>	if (chess[i][j] == 1)
>		return 0;
>}

Dacă toate verificările au fost trecute cu succes, returnăm 1, semnificând că poziția este sigură.
>return 1;

**Funcția random**

Această funcție returnează un număr aleatoriu între minim și maxim.
>return minim + rand() % (maxim - minim +1 );

**Funcția solve_queens**

Această funcție folosește backtracking-ul pentru a încerca să găsească o soluție plasând reginele pe tablă.

>int solve_queens(int chess[8][8], int row, int n, int start_column)
 - start_column reprezintă coloana de start pentru alegerea poziției aleatoare a coloanei pe care este plasată regina

Verifică dacă am ajuns la finalul tablei și toate reginele au fost plasate, returnând 1, deoarece s-a găsit o soluție
>if (row == n)
>       return 1;

Se intră prin fiecare coloană pentru rândul curent și se alege o coloană aleatorie de la start_column
>for (i = 0; i < n; i++)
>   column=(start_column + random (0, n-1)) %n;

Se verifică dacă plasarea reginei în această poziție este sigură, folosind funcția safe, iar dacă răspunsul este afirmativ plasează regina pe poziția curentă.
>if (safe(chess, row, column, n))
>   chess[row][column] = 1;

Folosind recursivitatea se încearcă să se plaseze următoarea regină pe rândul următor, iar dacă s-a găsit o soluție pe acel rând se va returna 1.
>if(solve_queens(chess, row + 1, n, start_column))
>    return 1;

Dacă nu s-a găsit o soluție pe următoarele rânduri se reinițializează poziția.
>chess[row][column] = 0;

Dacă nu s-a găsit nicio poziție sigură pentru regina curentă se returnează 0.
>return 0;

**Funcția print**

Afișează tabla de șah odată ce a fost găsită o soluție.

**Funcția solve**

Această funcție are rolul de funcție intermediară și ajută la gestionarea mai ușoară a codului.

Inițializarea tablei cu 0.
>int chess[8][8] = { 0 };

Verificarea găsirii unei soluții prin apelarea funcției solve_queens.
>if(!solve_queens(chess, 0, n, 0))

Apelarea funcției de print pentru a afișa soluția găsită.
>print(chess, n);

**Funcția main**

Această funcție este punctul de intrare în program și oferă un meniu simplu pentru utilizator ce îi permite fie să genereze o soluție, fie să iasă din program. 
Meniul se folosește de o buclă do-while atât pentru afișarea opțiunilor cât și pentru repetarea meniului până la ieșire.

În primul caz se apelează funția solve cu dimensiunea tablei pentru generarea unei soluții.
>case 1: solve(8);

În al doilea caz se afișează mesajul pentru ieșire și terminarea programului
>case 2: cout<<"Ati iesit din program.";

În cazul default se afișează un mesaj de eroare, în cazul în care utilizatorul a dat o opțiune invalidă.
>default: cout<<"Optiune invalida! Reintroduceti optiunea.";